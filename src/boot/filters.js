export default async ({ Vue }) => {
  Vue.filter('trim', function (value) {
    return value.toString().trim()
  })

  Vue.filter('price', function (value) {
    if (value > 0) {
      return new Intl.NumberFormat('ru-RU', { maximumFractionDigits: 2 }).format(value) + ' руб'
    }
    return null
  })

  Vue.filter('formatted', function (value) {
    switch (true) {
      case value instanceof Date:
        return new Intl.DateTimeFormat('ru-RU', { day: 'numeric', month: 'numeric', year: 'numeric' }).format(value)

      case typeof value === 'number':
        return new Intl.NumberFormat('ru-RU', { maximumFractionDigits: 2 }).format(value)

      case typeof value === 'string':
        // eslint-disable-next-line no-case-declarations
        const matched = value.match(/^(\d)(\d{3})(\d{3})(\d{2})(\d{2})$/)
        if (matched) {
          return ['+7 (', matched[2], ') ', matched[3], '-', matched[4], '-', matched[5]].join('')
        }
        return value

      default:
        return value
    }
  })

  Vue.filter('plural', function (value, titles) {
    const n = Array.isArray(value) ? value.length : value
    return titles[(n % 10 === 1 && n % 100 !== 11) ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2]
  })
}
