import axios from 'axios'
axios.defaults.baseURL = process.env.API_URL

if (typeof window === 'undefined') {
  require('localstorage-polyfill')
}

const token = localStorage.getItem('token')
if (token) {
  axios.defaults.headers.common.Authorization = 'Bearer ' + token
}

try {
  const regexp = new RegExp('.?' + process.env.BASE_DOMAIN.split(':')[0] + '$')
  axios.defaults.baseURL = [process.env.API_URL, window.location.hostname.replace(regexp, '') || 'msk'].join('')
} catch {}

axios.interceptors.response.use(function (response) {
  return response.data
}, function (error) {
  return Promise.reject(error)
})

axios.interceptors.request.use(function (request) {
  const uri = request.url.split('?')
  const params = new URLSearchParams(uri[1] || '')

  params.set('t', Math.round(new Date().getTime() / 1000000).toString())

  request.url = [uri[0], params.toString()].join('?')

  return request
}, function (error) {
  return Promise.reject(error)
})
