import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

console.log('process.env.IS_MOCK', process.env.IS_MOCK)
if (process.env.IS_MOCK) {
  const normalAxios = axios.create()
  const mock = new MockAdapter(axios)

  mock.onPost('/auth/phone')
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/auth.json')

      return [200, data]
    })

  mock.onPost('/auth/phone/code')
    .reply(config => {
      console.info('[MockCode]', config)
      let data = {}
      if (config.data !== '6666') {
        return [500, require('../mockJson/errorCode.json')]
      } else {
        data = require('../mockJson/token.json')
      }

      return [200, data]
    })

  mock.onGet('order/list')
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/orders.json')

      return [200, data]
    })
  mock.onGet(/\/product\/\d+/)
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/product.json')

      return [200, data]
    })

  mock.onGet('init')
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/init.json')

      return [200, data]
    })

  mock.onGet('customer')
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/userData.json')

      return [200, data]
    })

  mock.onGet('category/list')
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/categories.json')

      return [200, data]
    })

  mock.onGet(/\/category\/\d+\/products/)
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/products.json')

      return [200, data]
    })

  mock.onGet(/\/category\/\d+\/sorting/)
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/sorting.json')

      return [200, data]
    })

  mock.onGet(/\/category\/\d+\/filters/)
    .reply(config => {
      console.info('[Mock] ', config.url)
      const data = require('../mockJson/filters.json')

      return [200, data]
    })

  mock.onPost()
    .reply(config => normalAxios.post(config.url, JSON.parse(config.data)))
  mock.onGet()
    .reply(config => normalAxios.get(config.url))
}
