import axios from 'axios'
import Vue from 'vue'

const plugin = {
  install (Vue) {
    Vue.apiDadata = axios.create({
      baseURL: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/',
      headers: {
        Accept: 'application/json',
        Authorization: 'Token 36b5769c6205af37e2d5fa0753c4a86bad03b5a8'
      }
    })
  }
}

Vue.use(plugin)
