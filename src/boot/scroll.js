import Vue from 'vue'
import { scroll } from 'quasar'
const { getScrollTarget, setScrollPosition } = scroll

const plugin = {
  install (Vue) {
    Vue.scrollToElement = function (el) {
      console.log('scrollToElement', el)
      const target = getScrollTarget(el)
      const offset = el.offsetTop
      const duration = 200
      setScrollPosition(target, offset, duration)
    }
  }
}

Vue.use(plugin)
