export default function chunkArray (array, chunkSize) {
  const arr = array.slice()
  const results = []
  if (chunkSize < 1) {
    return results
  }
  while (arr.length) {
    results.push(arr.splice(0, chunkSize))
  }
  return results
}
