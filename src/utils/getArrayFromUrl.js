export default function getArrayFromUrl (url) {
  const ArrayFromUrl = url.split('/')
  return ArrayFromUrl.filter(element => (element !== '') && (element !== 'catalog'))
}
