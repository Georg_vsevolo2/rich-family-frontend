const cardsCountCalculate = (params) => {
  params = Object.assign({
    areaWidth: 0,
    margins: 10,
    cardWidth: 160
  }, params)
  if (params.areaWidth < 350) {
    return 1
  }
  const cards = Math.floor((params.areaWidth - params.margins) / params.cardWidth)
  const totalIndent = params.margins * cards - 1
  return cards * params.cardWidth + totalIndent < params.areaWidth ? cards : cards - 1
}

export default cardsCountCalculate
