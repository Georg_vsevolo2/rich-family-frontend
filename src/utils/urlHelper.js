function getChildren (categories, id, url) {
  return categories.filter(sub => sub.parent_id === id).map(sub => {
    sub.url = url + sub.slug + '/'
    getChildren(categories, sub.id, sub.url)
  })
}

export default function urlHelper (categories) {
  categories.filter(category => !category.parent_id).forEach(category => {
    category.url = '/catalog/' + category.slug + '/'
    getChildren(categories, category.id, category.url)
  })

  return categories
}
