import axios from 'axios'
import City from 'src/model/City'
import DeliveryType from 'src/model/DeliveryType'
import Shop from 'src/model/Shop'

export const NS_CITY = 'Cities'
export const LOAD_CITIES = 'loadCities'

export const CITY = 'currentCity'
export const CITIES = 'cityList'
export const SHOPS = 'shopList'
export const SHOP = 'shop'
export const SHOPS_BY_CITY = 'currentCity_Shop'

export const SET_SHOP = 'setShop'

export default {
  namespaced: true,
  state: () => {
    return {
      [CITIES]: new Map(),
      [SHOPS]: new Map(),
      [CITY]: null
    }
  },
  getters: {
    [CITIES]: state => Array.from(state[CITIES].values()),
    [SHOPS]: state => Array.from(state[SHOPS].values()),
    [SHOPS_BY_CITY]: state => Array.from(state[SHOPS].values())
      .filter(shop => { return shop.city.id === state[CITY].id }),
    [CITY]: state => state[CITY],
    [SHOP]: state => state[CITY].shop
  },
  actions: {
    [SET_SHOP]: async ({ commit, state }, shop) => {
      commit(SET_SHOP, shop)
    },
    [LOAD_CITIES]: async ({ commit }) => {
      return await axios.get('city/list')
        .then(body => body.data)
        .then(cities => commit(LOAD_CITIES, cities))
    }
  },
  mutations: {
    [SET_SHOP]: (state, shop) => {
      state[CITY].shop = shop
      state[SHOP] = shop
      localStorage.setItem('shop', shop.id)
    },
    [LOAD_CITIES]: (state, cities) => {
      const regexp = new RegExp('.?' + process.env.BASE_DOMAIN.split(':')[0] + '$')

      let HOST = ''
      if (typeof window !== 'undefined') {
        HOST = window.location.hostname.replace(regexp, '') || 'msk'
      }
      let localShop = null

      cities.map(city => {
        city.shops = city.shops.map(shop => {
          shop = Shop.from(shop)
          shop.city = city
          state[SHOPS].set(shop.id, shop)
          if (shop.id === parseInt(localStorage.getItem('shop'))) {
            localShop = shop
          }
          return shop
        })

        city = City.from(city)
        city.delivery = city.delivery.map(delivery => {
          return DeliveryType.from(delivery)
        })
        state[CITIES].set(city.id, city)

        if (HOST === city.domain) {
          state[CITY] = state[CITIES].get(city.id)
        }
      })
      if (state[CITY] === null) {
        state[CITY] = state[CITIES].get(1)
      }

      if (localShop) {
        state[CITY].shop = localShop
      } else {
        state[CITY].shop = state[CITY].shops
          .slice().reverse().pop()
      }
    }
  }
}
