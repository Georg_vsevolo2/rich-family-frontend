export const ASSIGN = 'assign'
export const SHOW_FILTERS = 'filtersView'
export const SHOW_SORT = 'sortView'
export const IS_BIG_CARD_SIZE = 'cardSize'

export default {
  namespaced: false,
  state: () => {
    return {
      [SHOW_FILTERS]: false,
      [SHOW_SORT]: false,
      [IS_BIG_CARD_SIZE]: false
    }
  },

  getters: {
    [SHOW_FILTERS]: state => state[SHOW_FILTERS],
    [SHOW_SORT]: state => state[SHOW_SORT],
    [IS_BIG_CARD_SIZE]: state => state[IS_BIG_CARD_SIZE]
  },

  actions: {
    [ASSIGN] ({ commit }, payload) {
      commit(ASSIGN, payload)
    },
    [SHOW_FILTERS] ({ commit }, isView) {
      commit(SHOW_FILTERS, isView)
    },
    [SHOW_SORT] ({ commit }, isView) {
      commit(SHOW_SORT, isView)
    },
    [IS_BIG_CARD_SIZE] ({ commit }, size) {
      commit(IS_BIG_CARD_SIZE, size)
    }
  },

  mutations: {
    [ASSIGN] (state, { obj, props }) {
      for (const key in props) {
        if (key in obj && Array.isArray(obj[key]) && !Array.isArray(props[key])) {
          obj[key].push(props[key])
        } else {
          obj[key] = props[key]
        }
      }
    },
    [SHOW_FILTERS] (state, isView) {
      state[SHOW_FILTERS] = !!isView
      state[SHOW_SORT] = false
    },
    [SHOW_SORT] (state, isView) {
      state[SHOW_SORT] = !!isView
      state[SHOW_FILTERS] = false
    },
    [IS_BIG_CARD_SIZE] (state, size) {
      state[IS_BIG_CARD_SIZE] = !!size
      state[SHOW_SORT] = false
      state[SHOW_FILTERS] = false
    }
  }
}
