import Vue from 'vue'
import { USER } from 'src/store/User'

export const NS_ORDER = 'Order'
export const ORDER = 'NEW_ORDER'
export const GET_SUGGESTIONS = 'GET_SUGGESTIONS'
export const ORDER_SUGGESTIONS = 'ORDER_SUGGESTIONS'
export const CHANGE_ORDER = 'changeOrder'

export default {
  namespaced: true,
  state: () => {
    return {
      [ORDER_SUGGESTIONS]: []
    }
  },
  getters: {
    [ORDER]: state => state[USER].order,
    [ORDER_SUGGESTIONS]: state => state[ORDER_SUGGESTIONS]
  },
  actions: {
    [CHANGE_ORDER]: async ({ commit }, order) => {
      return commit(CHANGE_ORDER, order)
    },
    [GET_SUGGESTIONS]: async ({ commit }, query) => {
      // TODO: kladr_id - получать id города/области
      const request = {}
      request.query = query
      request.count = 10
      request.locations_boost = [{ kladr_id: '5400000100000' }]
      request.from_bound = { value: 'city' }

      Vue.apiDadata.post('/address', request)
        .then((result) => {
          return result.data.suggestions.map(item => {
            return item.value
          })
        })
        .then(data => commit(GET_SUGGESTIONS, data))
        .catch((err) => {
          console.log(err)
        })
    }
  },
  mutations: {
    [GET_SUGGESTIONS]: (state, data) => {
      state[ORDER_SUGGESTIONS] = data
    },
    [CHANGE_ORDER]: (state, order) => {
      state[ORDER] = order
    }
  }
}
