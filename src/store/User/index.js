import axios from 'axios'

if (typeof window === 'undefined') {
  require('localstorage-polyfill')
}

import { User } from 'src/model/User'
import { Order } from 'src/model/Order'
import { Product } from 'src/model/Product'
import { CartItem } from 'src/model/Cart'

import { store } from 'src/store/'
import { LOAD_NEW_PRODUCT, NS_CATEGORY } from 'src/store/Category/const'

export const NS_USER = 'User'
export const USER = 'user'
export const CART = 'cart'
export const USER_PHONE_AUTH = 'authorizeByPhone'
export const USER_SEND_CODE = 'sendCode'
export const USER_AUTH = 'userAuth'
export const USER_DATA = 'userData'
export const GET_USER_DATA = 'getUserData'
export const USER_REVIEW_VOTES = 'userReviewVotes'

export const ADD_TO_CART = 'addToCart'
export const DELETE_FROM_CART = 'deleteFromCart'

export const ORDERS = 'ORDERS'
export const CREATE_NEW_ORDER = 'CREATE_NEW_ORDER'
export const CHANGE_ORDER = 'CHANGE_ORDER'
export const COMPARE = 'compare'
export const FAVORITE = 'favorite'

export const PRODUCT_WATCHED = 'watched'
export const PRODUCT_COMPARE = 'comparisons'
export const PRODUCT_FAVORITE = 'favorites'

export const USER_SET_STATE = 'setUser'
export const USER_STATUS = 'checkUserProducts'
export const CHANGE_USER_DATA = 'changeUserData'
export const LOAD_USER_DATA = 'loadUserData'
export const LOAD_ORDERS = 'loadOrders'

export const UPDATE_OFFER_CART = 'updateOfferInCart'

const localProducts = {
  cart: localStorage.getItem('cart'),
  favorites: localStorage.getItem('favorite'),
  comparisons: localStorage.getItem('compare')
}

function remoteUserProductChange ({ state, commit }, data) {
  const type = data.type
  const product = data.product
  const url = '/customer/' + type + '/' + product.id
  if (!state[USER][type].includes(product)) {
    axios.post(url)
      .then(() => {
        return commit(data.mutateType, product)
      })
  } else {
    axios.delete(url)
      .then(() => {
        return commit(data.mutateType, product)
      })
  }
  return new Promise(resolve => { resolve() })
}

export default {
  namespaced: true,
  state: () => {
    return {
      [USER]: new User()
    }
  },
  getters: {
    [USER]: state => state[USER],
    [CART]: state => state[USER].cart,
    [FAVORITE]: state => state[USER].favorite,
    [COMPARE]: state => state[USER].compare,
    [ORDERS]: state => state[USER].orders,
    [GET_USER_DATA]: state => state[USER_DATA],
    [USER_REVIEW_VOTES]: state => state[USER].votes
  },
  actions: {
    [USER_STATUS]: async ({ commit, state }) => {
      await axios.get('customer')
        .then(body => {
          // проверка авторизации; пользовательские данные ЛК
          if (!state[USER].isAuth) {
            const changes = {
              name: body.data.name,
              phone: body.data.phone
            }
            commit(USER_AUTH)
            commit(CHANGE_USER_DATA, changes)
            console.log('пользователь авторизован')
          }
          // товары с бэкэнда
          Object.keys(body.data).forEach(userProducts => {
            if (localProducts[userProducts] !== undefined) {
              let products = localProducts[userProducts]
              if (userProducts !== 'cart') {
                if (typeof products === 'string') {
                  products = localProducts[userProducts].split(',').map(product => parseInt(product))
                }
                if (products.length > 0) {
                  localProducts[userProducts] = Array.from(new Set([...products, ...body.data[userProducts]]))
                } else {
                  localProducts[userProducts] = body.data[userProducts]
                }
              }
            }
          })
        }).catch(() => {
          localStorage.removeItem('token')
          console.log('пользователь не авторизован')
        })
      commit(USER_SET_STATE)

      localProducts.watched = localStorage.getItem('watched')
      // товары с localStorage
      Object.keys(localProducts).forEach((userProducts, index, f) => {
        let products = localProducts[userProducts]
        if (userProducts === 'cart') {
          try {
            const items = JSON.parse(products)
            items.map((item, key, array) => {
              commit(ADD_TO_CART, CartItem.fromLocal(item))
            })
          } catch {}
        } else {
          if (typeof products === 'string') {
            products = products.split(',').filter(product => !!product)
          }
          try {
            products.map(productId => {
              const id = parseInt(productId)
              const product = Product.from({ id: id })
              if (userProducts === 'comparisons') {
                store.dispatch([NS_CATEGORY, LOAD_NEW_PRODUCT].join('/'), product).then(() => {
                  commit(userProducts, product, 0)
                })
              } else commit(userProducts, product, 0)
            })
          } catch {}
        }
      })
    },
    [CHANGE_USER_DATA]: async ({ commit }, changes) => {
      return commit(CHANGE_USER_DATA, changes)
    },
    [ADD_TO_CART]: async ({ commit }, offer) => {
      return commit(ADD_TO_CART, new CartItem({ offer: offer, count: 1 }))
    },
    [DELETE_FROM_CART]: async ({ commit }, cartItem) => {
      return commit(DELETE_FROM_CART, cartItem)
    },
    [USER_PHONE_AUTH]: async ({ commit }, phone) => {
      return axios.post('/customer/sms', { phone: phone })
    },
    [LOAD_USER_DATA]: async ({ commit }) => {
      // TODO: загрузка данных пользователя
      return commit(USER_DATA, [])
    },
    [CREATE_NEW_ORDER]: async ({ commit, state }, delivery) => {
      return commit(CREATE_NEW_ORDER, delivery)
    },
    [CHANGE_ORDER]: async ({ commit, state }, data) => {
      return commit(CHANGE_ORDER, data)
    },
    [LOAD_ORDERS]: async ({ commit }) => {
      return await axios.get('order/list')
        .then(body => body.data)
        .then(body => {
          body = body.map((order) => {
            // TODO: LOAD PRODUCT
            order.products = order.products.map(product => {
              return new CartItem({
                product: Product.from(product),
                count: product.count
              })
            })
            order.date = new Date(order.date)
            return Order.from(order)
          })
          return body
        })
        .then(data => commit(LOAD_ORDERS, { data }))
    },
    [USER_SEND_CODE]: async ({ state, commit }, sendData) => {
      return axios.post('/customer/auth', sendData)
        .then(body => body.token)
        .then(token => {
          localStorage.setItem('token', token)
        })
    },
    [PRODUCT_COMPARE]: async ({ commit, state }, product) => {
      if (state[USER].isAuth) {
        const data = { type: 'compare', mutateType: PRODUCT_COMPARE, product: product }
        return remoteUserProductChange({ commit, state }, data)
      }
      return commit(PRODUCT_COMPARE, product)
    },
    [PRODUCT_FAVORITE]: async ({ commit, state }, product) => {
      if (state[USER].isAuth) {
        const data = { type: 'favorite', mutateType: PRODUCT_FAVORITE, product: product }
        return remoteUserProductChange({ commit, state }, data)
      }
      return commit(PRODUCT_FAVORITE, product)
    },
    [PRODUCT_WATCHED]: async ({ commit }, product) => {
      return commit(PRODUCT_WATCHED, product)
    },
    [UPDATE_OFFER_CART]: async ({ commit }, { product, cartItem }) => {
      return commit(UPDATE_OFFER_CART, { product, cartItem })
    }
  },
  mutations: {
    [LOAD_ORDERS]: (state, data) => {
      state[USER].orders = data.data
    },
    [CHANGE_USER_DATA]: (state, changes) => {
      state[USER].profile = changes
    },
    [CREATE_NEW_ORDER]: (state, delivery) => {
      state[USER].order = Order.from(state[USER].cart)
      state[USER].order.status = 'Новый'
      state[USER].order.delivery = delivery
    },
    [CHANGE_ORDER]: (state, data) => {
      state[USER].order.delivery = data.delivery
      state[USER].order.delivery.address = data.address
    },
    [USER_SET_STATE]: (state) => {
      state[USER].ready = true
    },
    [USER_AUTH]: (state) => {
      state[USER].state = 'isAuth'
    },
    [USER_DATA]: (state, data) => {
      state[USER_DATA] = data
    },
    [DELETE_FROM_CART]: (state, cartItem) => {
      state[USER].cart.remove(cartItem)
    },
    [ADD_TO_CART]: (state, cartItem) => {
      state[USER].cart.add(cartItem)
    },
    [UPDATE_OFFER_CART]: (state, { product, cartItem }) => {
      const offerId = cartItem.offer.id
      try {
        cartItem.offer = product._offers.filter(offer => offer.id === offerId).shift()
      } catch {}
    },
    [PRODUCT_WATCHED]: (state, product, sync = 1) => {
      // state[USER].changeLocalProducts('watched', product, sync)
      // TODO: await обертку
      if (!state[USER].watched.includes(product)) {
        state[USER].watched.push(product)
      }
    },
    [PRODUCT_FAVORITE]: (state, product, sync = 1) => {
      state[USER].changeLocalProducts('favorite', product, sync)
    },
    [PRODUCT_COMPARE]: (state, product, sync = 1) => {
      state[USER].changeLocalProducts('compare', product, sync)
    }
  }
}
