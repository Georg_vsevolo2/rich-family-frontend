import Vue from 'vue'
import Vuex from 'vuex'

import Common from './Common'
import Order from './Order'
import Cities from './Cities'
import Category from './Category'
import User from './User'

Vue.use(Vuex)

let store = null
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      Common,
      Order,
      Cities,
      Category,
      User
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  store = Store

  return Store
}

export { store }
