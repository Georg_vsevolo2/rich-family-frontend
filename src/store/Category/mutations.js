import { LOADED_FULL, LOADED_PART } from 'src/model'

import {
  CATEGORIES,
  FOUND_PRODUCTS, SEARCH_PRODUCTS,
  SET_LOADED_STATE,
  LOAD_CATEGORIES,
  LOAD_FILTERS,
  LOAD_NEW_PRODUCT,
  LOAD_PRODUCT,
  LOAD_PRODUCTS,
  LOAD_SORTING, PRODUCT_FILL_RELATED, PRODUCTS,
  URLS, FILTER_SET, FILTER_RESET,
  PRODUCT_INIT, PRODUCT_SET_OFFER
} from 'src/store/Category/const'
import Vue from 'vue'

export default {
  menu: (state, val) => {
    state.menu = val
  },
  [PRODUCT_INIT]: (state, product) => {
    state[PRODUCTS].set(product.id, product)
  },
  [PRODUCT_SET_OFFER]: (state, { product, offer }) => {
    product._offerToCart = offer
  },
  [SET_LOADED_STATE]: (state, { mod: model, state: ModelState }) => {
    model.setState(ModelState)
  },
  [LOAD_NEW_PRODUCT]: (state, { product, data }) => {
    Object.keys(data).forEach(item => {
      product[item] = data[item]
    })
    if (!state[PRODUCTS].get(product.id)) {
      state[PRODUCTS].set(product.id, product)
    }
    if (!product.category.productExist(product)) {
      product.category.add(product)
    }
    product.setState(LOADED_FULL)
  },
  [LOAD_PRODUCT]: (state, { product, data }) => {
    Object.keys(data).forEach(item => {
      if (Array.isArray(product[item]) && Array.isArray(data[item])) {
        data[item].forEach(value => product[item].push(value))
      } else {
        product[item] = data[item]
      }
    })
    product.setState(LOADED_FULL)
  },
  [LOAD_CATEGORIES]: (state, data) => {
    data.forEach(category => {
      state[CATEGORIES].set(category.id, category)
      state[URLS].set(category.url, category)
    })
  },
  // @TODO loaded products should be change category state, to prevent loading products again
  [LOAD_PRODUCTS]: (state, { products }) => {
    products.forEach(product => {
      if (!state[PRODUCTS].get(product.id)) {
        state[PRODUCTS].set(product.id, product)
        product.category.add(product)
        product.setState(LOADED_PART)
      }
    })
  },
  [LOAD_SORTING]: (state, { category, sorting }) => {
    category.sorting = {
      price: sorting.price || [],
      pop: sorting.pop || [],
      new: sorting.new || []
    }
  },
  [LOAD_FILTERS]: (state, { category, filters }) => {
    Object.assign(category.filters, filters)
  },
  [PRODUCT_FILL_RELATED]: (state, product) => {
    product.category.products.filter(product_ => product_ !== product).slice(0, 6).forEach(product_ => product.recommended.push(product_))
  },

  [FILTER_SET]: (state, { filters, property }) => {
    if (!(property.name in filters.active)) {
      Vue.set(filters.active, property.name, {})
    }
    if (property.value in filters.active[property.name]) {
      Vue.delete(filters.active[property.name], property.value)
    } else {
      Vue.set(filters.active[property.name], property.value, property)
    }
    filters.category.filter()
  },

  [FILTER_RESET]: (state, { filters, id }) => {
    Vue.delete(filters.active, id)
    filters.category.filter()
  },

  filter: (state, category) => {
    category.filter()
  },

  [SEARCH_PRODUCTS]: (state, products) => {
    state[FOUND_PRODUCTS] = new Map()
    products.forEach(product => {
      if (!state[PRODUCTS].get(product.id)) {
        let category = product.category
        do {
          category.add(product)
          category = category.parent
        }
        while (category)
      }
      state[PRODUCTS].set(product.id, product)
      product.setState(LOADED_PART)
      state[FOUND_PRODUCTS].set(product.id, product)
    })
  }
}
