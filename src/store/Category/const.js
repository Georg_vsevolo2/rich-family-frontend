
export const NS_CATEGORY = 'Category'

export const CATEGORIES = 'Categories'
export const SET_LOADED_STATE = 'loadedState'
export const CATEGORY_MAP = 'CategoriesMap'
export const PRODUCTS = 'Products'

export const PRODUCT = 'Product'
export const PRODUCT_INIT = 'ProductInit'
export const PRODUCT_SET_OFFER = 'ProductSetOffer'

export const CATEGORY_BY_ID = 'categoryById'
export const CATEGORY_BY_SLUG = 'categoryBySlug'
export const CATEGORY_BY_URL = 'categoryByUrl'

export const PRODUCT_COLOR = 'productColor'
export const PRODUCT_SIZE = 'productSize'
export const PRODUCT_BY_URL = 'productByUrl'
export const PRODUCTS_BY_IDS = 'productsByIds'

export const URLS = 'urlset'

export const LOAD_CATEGORIES = 'loadCategories'
export const LOAD_PRODUCTS = 'loadCategoryProducts'
export const LOAD_NEW_PRODUCT = 'loadNewProduct'
export const LOAD_PRODUCT = 'loadOneProduct'
export const LOAD_SORTING = 'loadCategorySorting'
export const LOAD_FILTERS = 'loadCategoryFilters'

export const PRODUCT_FILL_RELATED = 'productFillRelated'

export const FOUND_PRODUCTS = 'SearchProd'
export const SEARCH_PRODUCTS = 'getProductsByQuery'

export const FILTER_SET = 'FilterSet'
export const FILTER_RESET = 'FilterReSet'

export const LIKE = 'like'
export const DISLIKE = 'dislike'
export const VOTE = 'vote'
