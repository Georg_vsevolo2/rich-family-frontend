import axios from 'axios'
import { Category } from 'src/model/Category'
import { LOAD_CITIES, NS_CITY } from 'src/store/Cities'
import { LOADED_ERROR, LOADED_FULL, LOADED_PART } from 'src/model'
import { Product, productArrayProxy } from 'src/model/Product'
import {
  CATEGORIES,
  FILTER_RESET,
  FILTER_SET,
  LOAD_CATEGORIES,
  LOAD_FILTERS,
  LOAD_NEW_PRODUCT,
  LOAD_PRODUCT,
  LOAD_PRODUCTS,
  LOAD_SORTING,
  PRODUCT_COLOR,
  PRODUCT_FILL_RELATED,
  PRODUCT_INIT,
  PRODUCT_SET_OFFER,
  SEARCH_PRODUCTS,
  SET_LOADED_STATE,
  VOTE
} from 'src/store/Category/const'
import Review from 'src/model/Review'
import { NS_USER, USER } from 'src/store/User'
import { ASSIGN } from 'src/store/Common'

export default {
  [PRODUCT_INIT]: ({ state, commit }, id) => {
    commit(PRODUCT_INIT, Product.from({ id: id }))
  },
  [LOAD_CATEGORIES]: async ({ commit, state }) => {
    return await axios.get('init')
      .then(body => body.data)
      .then(data => {
        data.categories = data.categories.map(data => Category.from(data))
        return data
      })
      .then(data => {
        data.categories = data.categories.map(category => {
          if (category.parent) {
            category.parent = data.categories.find(parent => parent.id === category.parent)
            category.parent.children.push(category)
          }
          return category
        })
        return data
      })
      .then(data => {
        commit(LOAD_CATEGORIES, data.categories)
        commit([NS_CITY, LOAD_CITIES].join('/'), data.cities, { root: true })
      })
  },

  [LOAD_PRODUCTS]: async ({ commit, state }, category) => {
    const url = ['category', category.id, 'products'].join('/')

    if (category.isFullLoaded === true) {
      return new Promise(resolve => { resolve() })
    }

    return await axios.get(url)
      .then(body => body.data)
      .then(products => {
        for (const n in products) {
          products[n].category = state[CATEGORIES].get(products[n].category)
          products[n].state = LOADED_PART
          products[n] = Product.from(products[n])
        }

        return products
      })
      .then(products => commit(LOAD_PRODUCTS, { products, category }))
      .then(() => Promise.all([]))
      .then(() => {
        commit(SET_LOADED_STATE, { mod: category, state: LOADED_FULL })
        console.log('*** catalog category load done')
      })
      .then(() => {
        commit('filter', category)
      })
  },

  [LOAD_NEW_PRODUCT]: async ({ commit, state }, product) => {
    if (product.isFullLoaded) { // product fullLoad
      return new Promise(resolve => { resolve() })
    }

    const url = ['product', product.id].join('/')
    return await axios.get(url)
      .then(body => body.data)
      .then(data => {
        data.category = state[CATEGORIES].get(data.category)
        data.properties = new Map((data.properties || []).map(prop => [prop.name, prop.value]))
        return data
      })
      .then(data => commit(LOAD_NEW_PRODUCT, { product, data }))
      .catch((error) => {
        console.log('Error loading product, ', error)
        commit(SET_LOADED_STATE, { mod: product, state: LOADED_ERROR })
      })
  },

  [LOAD_PRODUCT]: async ({ commit, state, getters }, product) => {
    const url = ['product', product.id].join('/')
    return await axios.get(url)
      .then(body => body.data)
      .then(data => {
        data.category = state[CATEGORIES].get(data.category)
        // data.recommendations = data.recommendations.map(id => state[PRODUCTS].get(id))
        data.reviews = [
          Review.from({ id: 15, product: product, like: 2, dislike: 1, name: 'Oleg', text: 'Lorem iphitecto, at atque dolore eni.', rating: 4 }),
          Review.from({ id: 18, product: product, rating: 2, like: 5, dislike: 2, name: 'Petr', text: 'Lorem  Architecto, at atque dolore enim explicabo fugiat fugit illo obcaecati placeat.' }),
          Review.from({ id: 16, product: product, rating: 1, like: 12, dislike: 9, name: 'Vasya', text: 'Lorem  Architecto, at atque dolore enim explicabo fugiat fugit illo obcaecati placeat.' })
        ]
        // data.colors = [122124, 122128, 122129]
        data.colors = new Proxy([122124, 122128, 122129], productArrayProxy)
        data.properties = new Map((data.properties || []).map(prop => [prop.name, prop.value]))
        return data
      })
      .then(data => commit(LOAD_PRODUCT, { product, data }))
  },

  [LOAD_SORTING]: async ({ commit }, category) => {
    const url = ['category', category.id, 'sorting'].join('/')

    return await axios.get(url)
      .then(body => body.data)
      .then(data => commit(LOAD_SORTING, { category, sorting: data }))
  },

  [LOAD_FILTERS]: async ({ commit }, category) => {
    const url = ['category', category.id, 'filters'].join('/')

    return await axios.get(url)
      .then(body => body.data)
      .then(data => Object.assign(data, { category: category }))
      .then(filters => commit(LOAD_FILTERS, { category, filters }))
  },
  [PRODUCT_FILL_RELATED]: ({ commit }, product) => {
    commit(PRODUCT_FILL_RELATED, product)
  },
  [PRODUCT_COLOR]: ({ commit }, { product, color }) => {
    commit(PRODUCT_COLOR, { product, color })
  },
  [PRODUCT_SET_OFFER]: ({ commit }, data) => {
    commit(PRODUCT_SET_OFFER, data)
  },
  [FILTER_SET]: async ({ commit }, { filters, property }) => {
    commit(FILTER_SET, { filters, property })
  },
  [FILTER_RESET]: async ({ commit }, { filters, id }) => {
    commit(FILTER_RESET, { filters, id })
  },

  [SEARCH_PRODUCTS]: async ({ commit, state }, q) => {
    const query = '?q=' + q
    const url = ['search', query].join('/')
    return await axios.get(url)
      .then(body => body.data)
      .then(products => {
        for (const n in products) {
          products[n].category = state[CATEGORIES].get(products[n].category)
          products[n].state = LOADED_PART
          products[n] = Product.from(products[n])
        }
        return products
      })
      .then(products => commit(SEARCH_PRODUCTS, products))
  },
  [VOTE]: ({ rootState, commit }, userVote) => {
    if (!rootState[NS_USER][USER].votes.find(vote => vote.review === userVote.review)) {
      const vote = userVote.value === 1 ? 'like' : 'dislike'
      commit(ASSIGN, { obj: userVote.review, props: { [vote]: userVote.review[vote] + 1 } }, { root: true })
      commit(ASSIGN, { obj: rootState[NS_USER][USER], props: { votes: userVote } }, { root: true })

      return axios.post(`/review/${userVote.review.id}`, userVote)
        .then()
        .catch(() => {
          setTimeout(() => {
            commit(ASSIGN, { obj: userVote.review, props: { [vote]: userVote.review[vote] - 1 } }, { root: true })
            commit(ASSIGN, {
              obj: rootState[NS_USER][USER],
              props: {
                votes: rootState[NS_USER][USER].votes.filter(vote =>
                  vote.review.id !== userVote.review.id
                )
              }
            }, { root: true })
          }, 3500
          )
        })
    }
  }
}
