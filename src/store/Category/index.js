import {
  CATEGORIES,
  FOUND_PRODUCTS,
  CATEGORY_BY_ID,
  CATEGORY_BY_SLUG,
  CATEGORY_BY_URL,
  PRODUCT_BY_URL,
  PRODUCTS, PRODUCTS_BY_IDS,
  URLS
} from 'src/store/Category/const'

import actions from './actions.js'
import mutations from './mutations.js'

export default {
  namespaced: true,
  state: () => {
    return {
      menu: false,
      [FOUND_PRODUCTS]: new Map(),
      [CATEGORIES]: new Map(),
      [PRODUCTS]: new Map(),
      [URLS]: new Map()
    }
  },

  getters: {
    [PRODUCTS]: state => state[PRODUCTS],
    [FOUND_PRODUCTS]: state => Array.from(state[FOUND_PRODUCTS].values()),
    menu: state => state.menu,
    [CATEGORIES]: state => Array.from(state[CATEGORIES].values()),
    [CATEGORY_BY_ID]: state => (id) => state[CATEGORIES].get(id),
    [CATEGORY_BY_SLUG]: state => (slug) => state[CATEGORIES].values().find(category => category.slug === slug),
    [CATEGORY_BY_URL]: state => (url) => state[URLS].get(url),
    [PRODUCT_BY_URL]: state => (url) => state[PRODUCTS].get(product => product.url === url),
    [PRODUCTS_BY_IDS]: state => (ids) => ids.map(id => state[PRODUCTS].get(id))
  },

  actions,
  mutations
}
