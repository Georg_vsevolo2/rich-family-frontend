import Model from 'src/model'

export class Price extends Model {
  constructor (props) {
    super(props)
    this.actual = 0
    this.old = null
    this.available = true
  }

  static from (data) {
    if (data.old === data.actual) {
      data.old = null
    }
    return super.from(data)
  }

  get discount () {
    if (!this.old) {
      return null
    }

    return 100 - Math.ceil(this.actual * 100 / this.old)
  }
}
