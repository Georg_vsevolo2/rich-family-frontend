import Model from 'src/model'
import { Offer } from 'src/model/Offer'
import { Product } from 'src/model/Product'
import { store } from 'src/store'
import { NS_CATEGORY, PRODUCT_INIT, PRODUCTS } from 'src/store/Category/const'

export default class Cart extends Model {
  constructor (props) {
    super(props)

    this._offers = []
    this._items = []
  }

  add (cartItem) {
    if (!(cartItem instanceof CartItem)) {
      throw new Error('Item to add should be a CartItem instance')
    }
    if (this.contains(cartItem.offer)) {
      this._items.filter(item => item.offer === cartItem.offer).shift().count++
      return
    }

    this._items.push(cartItem)
    this.setLocalProducts()
  }

  remove (cartItem) {
    let index
    index = this._offers.indexOf(cartItem.offer)
    if (index > -1) {
      this._offers.splice(index, 1)
    }

    index = this._items.indexOf(cartItem)
    if (index > -1) {
      this._items.splice(index, 1)
    }
    this.setLocalProducts()
  }

  setLocalProducts () {
    let offers = this._items.map(cartItem => {
      const offer = {}
      offer.product = {}
      offer.id = cartItem.offer.id
      offer.product.id = cartItem.offer.product.id
      offer.count = cartItem.count
      return offer
    })
    offers = JSON.stringify(offers)
    localStorage.setItem('cart', offers)
  }

  get offers () {
    return this._items.map(item => item.offer)
  }

  get products () {
    return this._items.map(item => item.offer.product)
  }

  contains (item) {
    if (item instanceof Offer) {
      return this.offers.includes(item)
    }
    if (item instanceof Product) {
      return this.products.includes(item)
    }

    return false
  }

  get sum () {
    return this._items.reduce((accumulator, cartItem) => accumulator + cartItem.price.actual * cartItem.count, 0)
  }

  get size () {
    return this._items.length
  }

  get length () {
    return this._items.length
  }

  get count () {
    return this._items.length
  }

  get items () {
    return this._items
  }
}

export class CartItem extends Model {
  constructor (props) {
    Object.assign({
      offer: null,
      count: 1
    }, props)
    super(props)

    if (!(this.offer instanceof Offer)) {
      throw new Error('Cart item offer should be a Offer')
    }
  }

  get price () {
    return this.offer.price
  }

  static fromLocal (item) {
    const offer = Offer.from({ id: item.id })
    offer.product = store.getters[[NS_CATEGORY, PRODUCTS].join('/')].get(item.product.id)
    if (!offer.product) {
      offer.product = Product.from({ id: item.product.id })
      store.commit([NS_CATEGORY, PRODUCT_INIT].join('/'), offer.product)
    }
    return new this({ offer: offer, count: item.count })
  }
}
