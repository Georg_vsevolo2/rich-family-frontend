import Model from 'src/model'

export class FilterItem extends Model {}

export class FilterCollection extends Model {
  constructor () {
    super()

    this.category = null
    this.active = {}

    this.products = {}
    this.offers = {}

    this.values = {}
    this.names = {}
  }
}
