import Model from 'src/model'

export default class City extends Model {
  constructor (props) {
    super(props)

    this.id = null
    this.name = null
    this.slug = null
    this.shops = []
    this.shop = null
    // TODO: mock типов доставки
    this.delivery = [
      {
        type: 'self',
        name: 'Самовывоз',
        price: null
      },
      {
        type: 'courier',
        name: 'Доставка курьером',
        price: 500
      },
      {
        type: 'express',
        name: 'Экспресс-доставка',
        price: 400
      }
    ]
  }

  get getCenterMap () {
    return this.shop.getGeoData
  }

  toJSON () {
    return {
      id: this.id,
      name: this.name,
      slug: this.slug,
      shops: this.shops.map(shop => shop.id),
      shop: this.shop ? this.shop.id : null,
      delivery: this.delivery
    }
  }
}
