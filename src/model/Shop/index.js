import Model from 'src/model'
import City from 'src/model/City'

export default class Shop extends Model {
  constructor (props) {
    super(props)

    this.id = null
    this.name = null
    this.geo = null
    this.city = new City()
    this.address = null
    this.phones = '89139003234'
    this.workTime = 'ЕЖД 10:00 - 20:00'
  }

  set image (v) {

  }

  get image () {
    return 'shops/' + this.id + '.webp'
  }

  get getGeoData () {
    let data = '54.84769,83.0637'
    if (this.geo !== null) {
      data = this.geo
    }
    return data.split(',')
  }

  get getPhones () {
    return this.phones.split(',')
  }

  get isActive () {
    return this.city.id === this.id
  }

  get fullAddress () {
    return [this.city.name, this.address].filter(v => !!v).join(', ')
  }
}
