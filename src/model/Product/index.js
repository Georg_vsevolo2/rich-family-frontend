import Model from 'src/model'
import { store } from 'src/store'
import { NS_USER, USER } from 'src/store/User'
import { NS_CITY, SHOP } from 'src/store/Cities'
import { Price } from 'src/model/Price'
import { Offer } from 'src/model/Offer'
import { NS_CATEGORY, PRODUCT_INIT, PRODUCTS } from 'src/store/Category/const'

export class Product extends Model {
  constructor () {
    super()
    this.id = null
    this.name = null
    this.image = null
    this.description = null
    this.slug = null
    this.properties = new Map()
    this.recommended = []
    this.reviews = []
    this.category = null
    this.delivery = []
    this.article = null
    this.store = null
    this.rating = null

    this.images = []

    this.related = new Proxy([], productArrayProxy)
    this.colors = []

    this._offers = []
    this._offerToCart = null
  }

  toString () {
    return this.id
  }

  toJSON () {
    return { id: this.id, name: this.name }
  }

  set offers (offers) {
    this._offers = offers.map(data => data instanceof Offer ? data : Offer.from(Object.assign(data, { product: this })))
  }

  get offers () {
    return this._offers.filter(offer => offer.prices.filter(price => price.shop === store.getters[[NS_CITY, SHOP].join('/')].id).length)
  }

  get sizes () {
    return this.offers.filter(offer => offer.name)
  }

  get loadText () {
    if (this.isErrorLoaded) {
      return 'Товар недоступен'
    }
  }

  get new () {
    return (this.id % 7 === 0)
  }

  get price () {
    const offer = this.offers.slice().shift() || null
    if (offer) {
      return offer.price
    }
    return new Price()
  }

  get isCompare () {
    return store.getters[[NS_USER, USER].join('/')].compare.includes(this)
  }

  get isFavorite () {
    return store.getters[[NS_USER, USER].join('/')].favorite.includes(this)
  }

  get getDelivery () {
    return [
      {
        name: 'Самовывоз',
        date: 'сегодня'
      },
      {
        name: 'Доставка курьером',
        date: 'завтра'
      },
      {
        name: 'Экспресс-доставка',
        date: 'сегодня'
      }
    ]
  }

  get url () {
    return [this.category.url, this.slug].join('/')
  }

  get getCharacters () {
    return Array.from(this.properties)
  }

  get getDefaultImage () {
    return 'img/no-photo-new.webp'
  }

  get assembly () {
    if (this.category.url.indexOf('velosipedy') > 0) {
      return 500
    }
    return null
  }

  get gallery () {
    if (this.images.length > 0) {
      return this.images
    } else {
      return [this.image]
    }
  }

  getSetSrc (imgSizeW, imgSizeH) {
    return [this.getSrc(imgSizeW, imgSizeH || imgSizeW), this.getSrc(imgSizeW * 2, (imgSizeH || imgSizeW) * 2) + ' 2x'].join(', ')
  }

  getSrc (imgSizeW, imgSizeH) {
    const parts = [process.env.IMG_URL, this.code, '/', this.code, '.jpg']
    if (imgSizeW) {
      parts.splice(-2, 0, imgSizeW, 'x', imgSizeH || imgSizeW, '/')
    }
    return parts.join('')
  }

  get inCart () {
    return store.getters[[NS_USER, USER].join('/')].cart.contains(this)
  }

  static from (data) {
    return Object.assign(new this(), data)
  }
}

export const productArrayProxy = {
  get: function (target, property) {
    if (typeof property !== 'symbol' && !isNaN(property) && !isNaN(target[property])) {
      let product = store.getters[[NS_CATEGORY, PRODUCTS].join('/')].get(target[property])
      if (!product) {
        product = Product.from({ id: target[property] })
        store.commit([NS_CATEGORY, PRODUCT_INIT].join('/'), product)
      }

      return product
    }
    return target[property]
  },
  set: function (target, property, value, receiver) {
    target[property] = value
    return true
  }
}
