import Model from 'src/model'

export const ORDER_STATUS_CANCELLED = 'Отменен'

export class Order extends Model {
  constructor (props) {
    super(props)

    this.status = null
    this.delivery = null
    this.payment = 'онлайн'
    this.contacts = null
    this._items = []
  }

  get isCancelled () {
    return this.status === ORDER_STATUS_CANCELLED
  }
}
