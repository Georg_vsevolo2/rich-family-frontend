import Model from 'src/model'
import { NS_CITY, SHOP } from 'src/store/Cities'
import { store } from 'src/store'
import { Price } from 'src/model/Price'
import { NS_USER, USER } from 'src/store/User'

export class Offer extends Model {
  constructor (props) {
    super(props)

    this.id = null
    this.product = null
    this._prices = []
    this.name = null
    this.value = null
    this.src = null
  }

  set prices (prices) {
    this._prices = prices.map(price => Price.from(price))
  }

  get inCart () {
    return store.getters[[NS_USER, USER].join('/')].cart.contains(this)
  }

  get prices () {
    const shop = store.getters[[NS_CITY, SHOP].join('/')]
    return this._prices.filter(price => price.shop === shop.id)
  }

  get price () {
    const shop = store.getters[[NS_CITY, SHOP].join('/')]

    return this._prices.find(price => price.shop === shop.id) || new Price()
  }

  get isAvailable () {
    return this.price.available
  }
}
