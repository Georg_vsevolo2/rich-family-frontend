export const INITIALIZED = 0x1
export const LOADED_PART = 0x2
export const LOADED_FULL = 0x4
export const LOADED_ERROR = 0x8

export default class Model {
  constructor (props) {
    Object.assign(this, props)
    this.state |= INITIALIZED
  }

  static from (data) {
    return Object.assign(new this(), data)
  }

  get isFullLoaded () {
    return Boolean(this.state & LOADED_FULL)
  }

  get isPartLoaded () {
    if (this.isFullLoaded) {
      return true
    }
    return Boolean(this.state & LOADED_PART)
  }

  get isErrorLoaded () {
    return Boolean(this.state & LOADED_ERROR)
  }

  setState (state) {
    this.state |= state
  }
}
