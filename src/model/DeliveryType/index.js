import Model from 'src/model'

export default class DeliveryType extends Model {
  constructor (props) {
    super(props)

    this.name = null
    this.address = null
    this.description = 'Доставка осуществляется в течение 5 часов с момента оплаты (при оплате с 08:00 до 17:00).\n' +
      'При оплате после 17:00 доставка будет осуществлена на следующий день до 14:00.'
    this.datetime = 'сегодня'
    this.area = null
    this.price = null
  }

  get priceCur () {
    if (this.price !== null) {
      return this.price
    } else {
      return 'Бесплатно'
    }
  }
}
