import Model from 'src/model'
import Vue from 'vue'
import { FilterCollection } from 'src/model/Filter'

export class Category extends Model {
  constructor () {
    super()

    this.parent = null
    this.children = []
    this.slug = null
    this.reviews = true

    this.sorting = {
      price: [],
      pop: [],
      new: []
    }

    this.prices = {
      min: null,
      max: null
    }

    this._products = []
    this.filters = new FilterCollection()

    this.filteredIds = this.products.map(product => product.id)

    Vue.observable(this)
  }

  toString () {
    return this.id
  }

  toJSON () {
    return { id: this.id, name: this.name }
  }

  get products () {
    if (!this.children.length) {
      return this._products.filter(product => product.offers.length)
    }

    let products = []
    this.children.forEach(category => {
      products = products.concat(category.products)
    })

    return products
  }

  set products (data) {
    this._products = data
  }

  productExist (product) {
    return this._products.includes(product)
  }

  get root () {
    if (this.parent) {
      return this.parent.root
    }

    return this
  }

  // get routeCategory () {
  //   const path = this.$route.path.split('/').filter(p => !!p)
  //   console.log('path', path)
  //   return path.pop()
  // }

  get parents () {
    const parents = [this]
    let parent = this.parent
    while (parent) {
      parents.push(parent)
      parent = parent.parent
    }

    return parents.reverse()
  }

  get parentIds () {
    const ids = [this.id]
    let parent = this.parent
    while (parent) {
      ids.push(parent.id)
      parent = parent.parent
    }

    return ids
  }

  get url () {
    const parts = [this.slug]
    if (this.parent) {
      parts.unshift(this.parent.url)
    } else {
      parts.unshift('/catalog')
    }

    return parts.join('/')
  }

  getChild (id) {
    return this.children.find(category => category.id === id)
  }

  getChildByParentIds (categoryIds) {
    return this.children.find(category => categoryIds.includes(category.id))
  }

  add (product) {
    this._products.push(product)
  }

  get fdv () {
    const fdv = {}

    for (const name in this.filters.active) {
      for (const value in this.filters.active[name]) {
        const prop = this.filters.active[name][value]

        fdv[prop.name] = fdv[prop.name] || []
        fdv[prop.name].push(prop.value)
      }
    }

    return fdv
  }

  filter () {
    if (this.filters === null) {
      this.filteredIds = this.products.map(product => product.id)
      return
    }
    const cfp = 'products' in this.filters ? this.filters.products : []
    const cfo = 'offers' in this.filters ? this.filters.offers : []
    console.log('FDV IS', this.fdv, cfp, cfo)
    this.filteredIds = this.products
      // filtering offers in current shop
      // .filter(product => product.offers.length)
      // filtering products by user filters
      .filter(product => {
        if (cfp.length && !(product.id in cfp)) {
          console.log('return false cfp.length ')
          return false
        }
        for (const k in this.fdv) {
          // product hasn't any values for filter key
          // so, we should check product.offers too
          if (!(product.id in cfp) || !(k in cfp[product.id])) {
            // product.offers hasn't any values for filter key too
            if (!product.offers.filter(offer => {
              if (!(offer.id in cfo) || !(k in cfo[offer.id]) || !Array.isArray(cfo[offer.id][k])) {
                console.log('155')
                return false
              }
              console.log('158')
              return this.fdv[k].filter(v => cfo[offer.id][k].includes(v)).length
            }).length) {
              console.log('161')
              return false
            }
          } else {
            if (!(product.id in cfp)) {
              console.log('166')
              return false
            }
            if (!Array.isArray(cfp[product.id][k])) {
              console.log('170')
              return false
            }
            // product property value is array of values
            // so we check all filters value that exists in product
            if (!this.fdv[k].filter(v => cfp[product.id][k].includes(v)).length) {
              console.log('176')
              return false
            }
          }
        }
        console.log('return true')
        return true
      })
      // .filter(product => (!this.prices.min || product.price >= this.prices.min) && (!this.prices.max || product.price <= this.prices.max))
      .map(product => product.id)
  }
}
