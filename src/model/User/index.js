import Model from 'src/model'
import Cart from 'src/model/Cart'
import { ROUTE_CART, ROUTE_COMPARE, ROUTE_FAVORITE, ROUTE_ORDERS, ROUTE_PRIVATE } from 'src/router/routes'

// localStorage.clear() // заглушка
export class User extends Model {
  constructor () {
    super()
    this.profile = []
    this.favorite = []
    this.compare = []
    this.votes = []

    this.cart = new Cart()
    this.order = null

    this.orders = []
    this.watched = []
    this.ready = false
  }

  get isAuth () {
    return this.state === 'isAuth'
  }

  changeLocalProducts (type, product, sync = 1) {
    if (!this[type].includes(product)) {
      this[type].push(product)
    } else {
      this[type] = this[type].filter(product_ => product_ !== product)
    }
    if (sync === 1) {
      this.setLocalProducts(type)
    }
  }

  setLocalProducts (type) {
    const ids = this[type].map(product => product.id)
    localStorage.setItem(type, ids)
  }

  get countCompareCategories () {
    const categories = []
    try {
      this.compare.map(product => {
        const category = product.category.root

        if (!categories.includes(category.id)) {
          categories.push(category.id)
        }
      })
    } catch {}
    return categories.length
  }

  get slider () {
    // TODO: remove temporary slider
    return [
      {
        id: 0,
        name: 'Большой выбор игрушек для мальчиков',
        description: 'В каталоге игрушек для мальчиков представлено более \n' +
          '640 товаров. Найдите своему ребенку такую игрушку, \n' +
          'которая не сможет его не порадовать.',
        src: '/img/new/boys.png',
        background: 'background: linear-gradient(72.95deg, #FFD600 0%, rgba(255, 138, 0, 0.85) 55.88%, #FFAF00 99.35%);'
      },
      {
        id: 1,
        name: 'Большой выбор игрушек для девочек',
        description: 'В каталоге игрушек для девочек представлено более \n' +
          '640 товаров. Найдите своему ребенку такую игрушку, \n' +
          'которая не сможет его не порадовать.',
        src: '/img/new/girls.png',
        background: 'background: #F47351;'
      }
    ]
  }

  get tabs () {
    const tabs = {
      cart: {
        name: 'Моя корзина',
        count: this.cart.length,
        path: { name: ROUTE_CART }
      },
      orders: {
        name: 'Заказы',
        count: this.orders.length,
        path: { name: ROUTE_ORDERS }
      },
      favorite: {
        name: 'Отложенные товары',
        count: this.favorite.length,
        path: { name: ROUTE_FAVORITE }
      },
      compare: {
        name: 'Сравнение товаров',
        count: this.compare.length,
        path: { name: ROUTE_COMPARE }
      },
      private: {
        name: 'Личные данные',
        path: { name: ROUTE_PRIVATE }
      }
    }
    if (!this.isAuth) {
      return {
        cart: tabs.cart,
        favorite: tabs.favorite,
        compare: tabs.compare
      }
    }
    return tabs
  }
}

export class UserVote extends Model {
  constructor (review, value) {
    super()

    this.review = review
    this.value = value
  }

  toJSON () {
    return {
      value: this.value
    }
  }
}
