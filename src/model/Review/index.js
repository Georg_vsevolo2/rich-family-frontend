import Model from 'src/model'

export default class Review extends Model {
  constructor (props) {
    super(props)

    this.id = null
    this.product = null
    this.user = null
    this.text = ''
    this.date = new Date()
    this.rating = 0
    this.like = 0
    this.dislike = 0
  }

  toJSON () {
    return {
      text: this.text,
      rating: this.rating
    }
  }
}
