import { store } from 'src/store'
import { LOAD_CATEGORIES, NS_CATEGORY } from 'src/store/Category/const'
import { NS_USER, USER_STATUS } from 'src/store/User'

export const ROUTE_HOME = 'route_home'
export const ROUTE_ROOT = 'ROUTE_ROOT'
export const ROUTE_404 = 'route_404'

export const ROUTE_CATALOG = 'ROUTE_CATALOG'
export const ROUTE_CART = 'ROUTE_CART'
export const ROUTE_ORDER = 'ROUTE_ORDER'
export const ROUTE_ORDERS = 'ROUTE_ORDERS'
export const ROUTE_ORDER_PAGE = 'ROUTE_ORDER_PAGE'
export const ROUTE_PRIVATE = 'ROUTE_PRIVATE'
export const ROUTE_FAVORITE = 'ROUTE_FAVORITE'
export const ROUTE_COMPARE = 'ROUTE_COMPARE'
export const ROUTE_STATIC = 'ROUTE_STATIC'
export const ROUTE_STATIC_RULES = 'ROUTE_STATIC_RULES'
export const ROUTE_ORDER_SUCCESS = 'ROUTE_ORDER_SUCCESS'
export const ROUTE_CATALOG_CATEGORY = 'ROUTE_CATALOG_CATEGORY'
export const ROUTE_CATALOG_SEARCH = 'ROUTE_CATALOG_SEARCH'
export const ROUTE_CATALOG_PRODUCT = 'ROUTE_CATALOG_PRODUCT'

const routes = [
  {
    path: '/',
    name: ROUTE_ROOT,
    redirect: { name: ROUTE_HOME },
    component: () =>
      store.dispatch([NS_CATEGORY, LOAD_CATEGORIES].join('/'))
        .then(() => console.log('category load done'))
        .then(() => store.dispatch([NS_USER, USER_STATUS].join('/')))
        .then(() => import('layouts/index'))
        .catch(e => console.log(e)),
    children: [
      {
        name: ROUTE_HOME,
        path: '',
        component: () => import('pages/new/Home')
      },
      {
        name: ROUTE_CART,
        path: 'Profile/cart',
        component: () => import('pages/User/cart')
      },
      {
        name: ROUTE_ORDER_SUCCESS,
        path: 'order-success',
        component: () => import('pages/User/order-success')
      },
      {
        name: ROUTE_ORDER,
        path: 'order',
        component: () => import('pages/User/order')
      },
      {
        name: ROUTE_ORDERS,
        path: 'Profile/orders',
        component: () => import('pages/User/orders'),
        children: [
          {
            name: ROUTE_ORDER_PAGE,
            path: 'Profile/orders/:id',
            component: () => import('pages/User/orders')
          }
        ]
      },
      {
        name: ROUTE_PRIVATE,
        path: 'profile',
        component: () => import('pages/User/Profile/')
      },
      {
        name: ROUTE_FAVORITE,
        path: 'Profile/favorite',
        component: () => import('pages/User/favorite')
      },
      {
        name: ROUTE_COMPARE,
        path: 'Profile/compare',
        component: () => import('pages/User/compare')
      },
      {
        name: ROUTE_CATALOG,
        path: 'catalog/*',
        component: () => import('pages/Catalog')
      },
      {
        name: ROUTE_CATALOG_SEARCH,
        path: 'search',
        component: () => import('pages/Catalog/Search')
      },
      {
        name: ROUTE_STATIC,
        path: 'company',
        component: () => import('pages/Static/company')
      },
      {
        path: 'advantages',
        component: () => import('pages/Static/advantages')
      },
      {
        path: 'delivery',
        component: () => import('pages/Static/delivery')
      },
      {
        path: 'payment',
        component: () => import('pages/Static/payment')
      },
      {
        path: 'return',
        component: () => import('pages/Static/return')
      },
      {
        path: 'returnPayment',
        component: () => import('pages/Static/returnPayment')
      },
      {
        name: ROUTE_STATIC_RULES,
        path: 'rules',
        component: () => import('pages/Static/rules')
      },
      {
        path: 'shops',
        component: () => import('pages/Static/shops')
      },
      {
        path: 'commercial',
        component: () => import('pages/Static/commercial')
      },
      {
        path: 'contacts',
        component: () => import('pages/Static/contacts')
      },
      {
        path: 'law-entities',
        component: () => import('pages/Static/law-entities')
      },
      {
        path: '*',
        name: ROUTE_404,
        component: () => import('pages/Error404.vue')
      }
    ]
  }
]

export default routes
